<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController');

Route::get('candidates/delete/{id}','CandidatesController@destroy') ->name('candidate.delete');

Route::get('candidates/changeuser/{cid}/{uid?}','CandidatesController@changeUser') ->name('candidate.changeuser')->middleware('auth');

Route::get('candidates/changestatus/{cid}/{sid}','CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth');

Route::get('/hello', function(){
    return 'Hello Larevel';
} );

Route::get('/student/{id}', function($id = 'No student found'){
    return 'we go student with id '.$id;
} );

Route::get('/car/{id?}', function ($id = null){
    if(isset($id)){
        return "we got car $id";
    } else{
        return 'we need the id to find your car';
    }
   
} );

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});

Route::get('/usersex5/{email}/{name?}', function ($email, $name = null) {
    if(filter_var($email, FILTER_VALIDATE_EMAIL)&& $name != null){
        return view('usersex5', compact('email','name'));

    }elseif (filter_var($email, FILTER_VALIDATE_EMAIL)&& $name == null){
        $name ='name is missing';
        return view('usersex5', compact('email', 'name'));
    }
        
   
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
