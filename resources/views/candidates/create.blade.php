@extends('layouts.app')

@section('title','Create candidate')

@section('content')
    
  
        <h1> create candidate <h1/>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div class="form-group"> 
            <lable for = "name">Candidate name</lable>
            <input type = "text" class="form-control "name = "name">  
        </div>
        <div class="form-group">
            <lable for = "email">Candidate email</lable>
            <input type = "text" class="form-control "name = "email">  
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Create candidate">
        </div>
       </form>         
            
  
@endsection
