@extends('layouts.app')

@section('title','Edit candidate')

@section('content')
   
        <h1> Edit candidate </h1>
        <form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
        @csrf
        @METHOD('PATCH')
        <div>
            <input type = "hidden" name = "_method" value = "PATCH">
        </div>
        <div class="form-group">
            <lable for = "name">Candidate name</lable>
            <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>  
        </div>
        <div class="form-group">
            <lable for = "email">Candidate email</lable>
            <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>  
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Update candidates"></div>
        

       </form>         
     
    
@endsection
